Bună ziua!
 Testul a fost realizat în JavaScript, mediul de rulare Node.js, framework express, baza de date MYSQL si ORM sequelize.
Am realizat subpunctele pentru back-end deoarece partea de front-end nu este punctul meu forte, însă sunt motivat să mă dezvolt și pe această latură! :)

Steps:

1) npm install in folderul back-end (în terminal)
2) deschidem XAMPP-ul și dăm start la Apache si MYSQL (de asemenea, intrăm în Admin și creăm o bază de date cu numele de "infoworld")
3) nodemon server.js
4) accesăm ruta de reset cu "localhost:8081/reset", pentru a ne face tabela "produs" in baza de date
