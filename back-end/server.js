const express = require("express");
const connection = require("./models").connection;
const router = require("./routes");

const app = express();

app.use(express.json());

let port = 8081;

app.use("/api", router);

app.get("/reset", (req, res) => {
  connection
    .sync({ force: true })
    .then(() => {
      res.status(201).send({
        message: "Database reset",
      });
    })
    .catch(() => {
      res.status(500).send({
        message: " Reset DB error",
      });
    });
}); // eventual sa-l bag in controllers, sa fac un fisier "other.js"

//app.use("/", express.static("../front-end/"));

app.listen(port, () => {
  console.log("Server is running on port: " + port);
});
