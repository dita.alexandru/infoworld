const express = require("express");
const router = express.Router();
const produseController = require("../controllers").produse;

router.post("/", produseController.addProdus);
router.get("/", produseController.getAllProduse);
router.get("/:id", produseController.getProdusById);
router.delete("/:id", produseController.deleteProdusById);
router.put("/", produseController.updateProdus);

module.exports = router;
