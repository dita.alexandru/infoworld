const express = require("express");
const router = express.Router();
const produseRouter = require("./produse");

router.use("/produse", produseRouter);

module.exports = router;
