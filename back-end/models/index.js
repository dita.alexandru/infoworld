const Sequelize = require("sequelize");
const db = require("../config").db;
const ProduseModel = require("./produse");

const produse = ProduseModel(db, Sequelize);

module.exports = {
  produse,
  connection: db,
};
