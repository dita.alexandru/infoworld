module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "produse",
    {
      nume: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      pret: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      cantitate: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      descriere: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      comentarii: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      rating: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
    },
    {
      underscored: true,
      freezeTableName: true,
    }
  );
};
