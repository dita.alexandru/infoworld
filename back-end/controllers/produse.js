const ProduseDb = require("../models").produse;

const controller = {
  addProdus: async (req, res) => {
    const produs = {
      nume: req.body.nume,
      pret: req.body.pret,
      cantitate: req.body.cantitate,
      descriere: req.body.descriere,
      comentarii: req.body.comentarii,
      rating: req.body.rating,
    };

    let errors = [];
    if (!produs.nume || !produs.pret) {
      errors.push("Nu au fost completate toate câmpurile obligatorii!");
      console.log("Nu au fost completate toate câmpurile obligatorii!");
    } else {
      if (produs.descriere != null) {
        if (produs.descriere.length > 500) {
          errors.push("Descrierea trebuie să conțină maxim 500 de cuvinte!");
          console.log("Descrierea trebuie să conțină maxim 500 de cuvinte!");
        }
      }

      if (produs.comentarii != null) {
        if (produs.comentarii.length > 500) {
          errors.push(
            "Câmpul de comentarii trebuie să conțină maxim 500 de cuvinte!"
          );
          console.log(
            "Câmpul de comentarii trebuie să conțină maxim 500 de cuvinte!"
          );
        }
      }

      if (produs.nume.length > 100) {
        errors.push("Câmpul de nume trebuie să conțină maxim 100 de cuvinte!");
        console.log("Câmpul de nume trebuie să conțină maxim 100 de cuvinte!");
      }
    }

    if (errors.length === 0) {
      ProduseDb.create(produs)
        .then((produs) => {
          res.status(201).send(produs);
        })
        .catch((err) => {
          console.log(err);
          res.status(500).send({ message: "Server Error!" });
        });
    } else {
      res.status(400).send({ message: "Ai completat greșit!" });
    }
  },

  getProdusById: async (req, res) => {
    const id = req.params.id;
    try {
      let produs = await ProduseDb.findByPk(id);
      if (!produs) throw new Error("gol");

      res.status(200).send(produs);
    } catch (err) {
      if (err.message === "gol")
        res.status(404).send({ message: `Nu există produsul cu id-ul ${id}` });
      else {
        console.log(err.message);
        res.status(500).send({ message: "Server error!" });
      }
    }
  },

  deleteProdusById: async (req, res) => {
    const { id } = req.params;
    ProduseDb.findByPk(id)
      .then((produs) => {
        if (produs) {
          produs.destroy().then(() => {
            res.status(200).send({ message: "Produs șters!" });
          });
        } else {
          res.status(404).send({ message: "Id produs inexistent!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server Error!" });
      });
  },

  getAllProduse: async (req, res) => {
    ProduseDb.findAll({
      order: [
        ["id", "DESC"],
        ["nume", "ASC"],
      ],
      attributes: ["id", "nume", "pret", "rating"],
    })
      .then((produse) => {
        res.status(200).send(produse);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server Error!" });
      });
  },

  updateProdus: async (req, res) => {
    const produs = await ProduseDb.findOne({
      where: {
        id: req.body.id,
      },
    });
    if (produs) {
      produs
        .update({
          nume: req.body.nume,
          pret: req.body.pret,
          cantitate: req.body.cantitate,
          descriere: req.body.descriere,
          comentarii: req.body.comentarii,
          rating: req.body.rating,
        })
        .then((produs) => res.status(200).send(produs))
        .catch((err) => res.status(500).send(err));
    } else
      res.status(400).send({ message: `Nu există produsul cu id-ul ${id}!` });
  },
};

module.exports = controller;
