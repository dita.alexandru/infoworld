const Sequelize = require("sequelize");

const sequelize = new Sequelize("infoworld", "root", "", {
  dialect: "mysql",
  host: "localhost",
  define: {
    timestamps: true,
  },
});

module.exports = sequelize;
